'use strict'
import React from 'react'
import {
  View,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
  Modal, Dimensions
} from 'react-native'
//Libraries
import { Field, reduxForm, reset } from 'redux-form'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Entypo'
import Feather from 'react-native-vector-icons/Feather'
//Assets
import { VALER_PURPLE, VALER_TEXT, VALER_TEXTFIELD, VALER_BACKGROUND } from '../../Assets/GlobalStyle'
import { signUpRequest, loginRequest, clearAlertRequest } from '../../Actions';
import { ReduxFormItemTextField } from '../../Assets/ReduxFormItemTextField.js'
import { RequiredValidation, EmailValidation, PasswordValidation } from '../../Assets/Validations.js'
import { Button } from '../../SettingsTab/Templates/Button'
import { LoadWheel } from '../../Assets/LoadWheel'
import RemoteSubmitButton from '../../Assets/RemoteSubmitButton.js'

let background = require('../../Assets/Login/login-bg.png')
let email = require('../../Assets/Login/email.png')
let password = require('../../Assets/Login/password.png')
let self
const { width } = Dimensions.get('window')
class SignUp extends React.Component {

  state = {
    isError: false,
    signUpError: '',
    isLoading: false,
    ErrorMessage: '',
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    businessName: '',
    businessUsername: '',
    emailFocused: false,
    focusField: ''
  }

  static navigationOptions = {
    title: 'Sign Up',
    headerStyle: { backgroundColor: VALER_PURPLE },
    headerTintColor: 'white',
    headerTitleStyle: { alignSelf: 'center' },
    //  header: null,
    //  headerBackTitle: null,
    headerLeft:
      <TouchableOpacity style={{ marginRight: 5 }} onPress={() => self.gotoScreen()} hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }} >
        <Icon name="chevron-small-left" size={35} color="#ffffff" />
      </TouchableOpacity>,
    headerRight: <RemoteSubmitButton text={"Sign Up"} formName={"signup"} />
  }

  gotoScreen() {
    //  this.props.navigation.goBack();
    this.props.navigation.navigate('Main');
  }

  goBack() {
    this.props.reset('signup');
    this.props.navigation.navigate('Main');
    // this.props.navigation.goBack();
  }

  componentDidMount() {
    self = this;
  }

  async componentWillReceiveProps(nextProps) {
    if (nextProps.signUpError || nextProps.loginError) {
      this.setState({ isLoading: false });
      setTimeout(() => { this.setState({ isError: true, errorMessage: nextProps.signUpError || nextProps.loginError }) }, 300);
    }
    else if (nextProps.signUpSuccess) {
      await this.setState({ isLoading: false, isError: false, errorMessage: '' }, () => {
        if (this.state.isLoading == false) {
          this.props.reset('signup');
          //  //  nextProps.resetForm();
          //this.props.navigation.navigate('Main')
        }
      });
      // await this.setState({isLoading: false});
      // this.props.reset('signup');
      //  setTimeout(() => {this.props.navigation.navigate('InventoryNavigation')}, 500);
      //  setTimeout(() => {this.props.reset('signup')}, 500);
    }
    //  else if(nextProps.loginSuccess) {
    //   await this.setState({isLoading: false, isError: false, errorMessage: ''});
    //   this.props.reset('login');
    //   this.props.navigation.navigate('Main');
    // }
  }
  onSignUpButtonPress(values, dispatch, props) {
    this.setState({ isLoading: true });
    let user = {};
    user['email'] = values.Email;
    user['password'] = values.Password;
    user['businessName'] = values.BusinessName;
    user['businessUsername'] = values.BusinessUsername;
    user['firstName'] = values.FirstName;
    user['lastName'] = values.LastName;
    this.props.signUpRequest(user);

  }
  clearAlertMessage() {
    this.props.clearAlertRequest();
    this.setState({ isError: false, isLoading: false })

  }
  render() {
    const { error, handleSubmit, dispatch } = this.props
    return (
      <View style={{flex:1}}>
        <View style={{ height: 70, width: width, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#3D3882', alignItems: 'center', paddingHorizontal: 5 }}>
          <TouchableOpacity onPress={() => this.goBack()}>
            <Icon name="chevron-left" style={{ backgroundColor: 'rgba(52, 52, 52, 0.0)', marginTop: 18 }} color='white' size={30} />
          </TouchableOpacity>
          <Text style={{ color: "white", marginTop: 10, fontSize: 20, marginLeft: 15 }}>Sign Up</Text>
          <RemoteSubmitButton text={"Save"} formName={"signup"} />
        </View>
        <ScrollView style={{ backgroundColor: VALER_BACKGROUND }}>
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <KeyboardAvoidingView behavior='padding'>
              <Text style={{ color: VALER_TEXTFIELD, marginLeft: 10, marginBottom: 3, paddingTop: 20 }}>LOGIN INFO</Text>
              <View style={{ paddingLeft: 10, paddingTop: 8, backgroundColor: 'white' }}>
                <View style={{ flexDirection: 'row' }}>
                  <Feather name="mail" style={{ marginTop: 10, marginRight: 10, backgroundColor: 'rgba(52, 52, 52, 0.0)' }} color={VALER_TEXTFIELD} size={25} />
                  <Field
                    name="Email"
                    label='Email'
                    component={ReduxFormItemTextField}
                    returnKeyType={'next'}
                    focus
                    withRef
                    ref={(componentRef) => this.Email = componentRef}
                    onFocus={(event) => this.setState({ emailFocused: true, focusField: 'Email' })}
                    onBlur={(event) => this.setState({ emailFocused: false })}
                    renderAccessory={() => this.state.emailFocused ? <TouchableOpacity onPress={() => this.Email.getRenderedComponent().refs.Email.clear()}><Icon size={16} color='white' /></TouchableOpacity> : undefined}
                    refField="Email"
                    onSubmitEditing={() => { this.Password.getRenderedComponent().refs.Password.focus() }}
                    secureTextEntry={false}
                    value={this.state.email}
                    onChange={(text) => { this.setState({ email: text }) }}
                    validate={[RequiredValidation, EmailValidation]}
                  />
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Feather name="lock" style={{ marginTop: 7, marginRight: 10, backgroundColor: 'rgba(52, 52, 52, 0.0)' }} color={VALER_TEXTFIELD} size={25} />
                  <Field
                    name="Password"
                    label='Password'
                    component={ReduxFormItemTextField}
                    returnKeyType={'next'}
                    focus
                    withRef
                    onFocus={(event) => this.setState({ focusField: 'Password' })}
                    ref={(componentRef) => this.Password = componentRef}
                    refField="Password"
                    onSubmitEditing={() => { this.FirstName.getRenderedComponent().refs.FirstName.focus() }}
                    secureTextEntry={true}
                    value={this.state.password}
                    onChange={(text) => { this.setState({ password: text }) }}
                    validate={[RequiredValidation, PasswordValidation]}
                  />
                </View>
              </View>
              <Text style={{ color: VALER_TEXTFIELD, marginLeft: 10, marginBottom: 3, paddingTop: 20 }}>PERSONAL INFO</Text>
              <View style={{ paddingLeft: 10, paddingTop: 8, backgroundColor: 'white' }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 2 }}>
                    <Field
                      name="FirstName"
                      label='First Name'
                      component={ReduxFormItemTextField}
                      returnKeyType={'next'}
                      focus
                      withRef
                      onFocus={(event) => this.setState({ focusField: 'FirstName' })}
                      ref={(componentRef) => this.FirstName = componentRef}
                      refField="FirstName"
                      onSubmitEditing={() => { this.LastName.getRenderedComponent().refs.LastName.focus() }}
                      secureTextEntry={false}
                      value={this.state.firstName}
                      onChange={(text) => { this.setState({ firstName: text }) }}
                      validate={[RequiredValidation]}
                    />
                  </View>
                  <View style={{ flex: 2, marginLeft: 8 }}>
                    <Field
                      name="LastName"
                      label='Last Name'
                      component={ReduxFormItemTextField}
                      returnKeyType={'next'}
                      focus
                      withRef
                      onFocus={(event) => this.setState({ focusField: 'LastName' })}
                      ref={(componentRef) => this.LastName = componentRef}
                      refField="LastName"
                      onSubmitEditing={() => { this.BusinessName.getRenderedComponent().refs.BusinessName.focus() }}
                      secureTextEntry={false}
                      value={this.state.lastName}
                      onChange={(text) => { this.setState({ lastName: text }) }}
                      validate={[RequiredValidation]}
                    />
                  </View>
                </View>
              </View>
              <Text style={{ color: VALER_TEXTFIELD, marginLeft: 10, marginBottom: 3, paddingTop: 20 }}>Business Info</Text>
              <View style={{ paddingLeft: 10, paddingTop: 8, backgroundColor: 'white' }}>
                <Field
                  name="BusinessName"
                  label='Business Name'
                  component={ReduxFormItemTextField}
                  returnKeyType={'next'}
                  focus
                  withRef
                  onFocus={(event) => this.setState({ focusField: 'BusinessName' })}
                  ref={(componentRef) => this.BusinessName = componentRef}
                  refField="BusinessName"
                  onSubmitEditing={() => { this.BusinessUsername.getRenderedComponent().refs.BusinessUsername.focus() }}
                  onChange={(text) => { this.setState({ businessName: text }) }}
                  value={this.state.businessName}
                  validate={[RequiredValidation]}
                />
                <Field
                  name="BusinessUsername"
                  label='Business Username'
                  component={ReduxFormItemTextField}
                  prefix='@'
                  returnKeyType={'done'}
                  focus
                  withRef
                  onFocus={(event) => this.setState({ focusField: 'BusinessUsername' })}
                  ref={(componentRef) => this.BusinessUsername = componentRef}
                  refField="BusinessUsername"
                  value={this.state.businessUsername}
                  onChange={(text) => { this.setState({ businessUsername: text }) }}
                  onSubmitEditing={handleSubmit(this.onSignUpButtonPress.bind(this))}

                  validate={[RequiredValidation]}
                />
              </View>
              <LoadWheel visible={this.state.isLoading} onRequestClose={() => this.setState({ isLoading: false })} text={'Login Up...'} />
              <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.isError}
                onRequestClose={() => { this.setState({ isError: false }) }}
              >
                <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>
                    <Text style={{ textAlign: 'center', margin: 10, fontSize: 18 }}> Registration Error</Text>
                    <Text style={{ textAlign: 'center', margin: 10, fontSize: 15, color: 'grey' }}> {this.state.errorMessage} </Text>
                    <TouchableOpacity onPress={this.clearAlertMessage.bind(this)}>
                      <View style={{ marginTop: 20 }}>
                        <Button
                          Label='Try Again'
                          BackgroundColor={{ backgroundColor: 'red' }}
                          Color={{ color: 'white' }}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </ScrollView>
      </View>
    )
  }
}

SignUp = reduxForm({
  // a unique name for the form
  form: 'signup',
  onSubmit: (values, dispatch, props) => {
    self.onSignUpButtonPress(values, dispatch, props);
  },

})(SignUp)

const mapStateToProps = ({ state, auth }) => {
  return auth;
}

export default connect(mapStateToProps, { signUpRequest, loginRequest, clearAlertRequest, reset })(SignUp);
