import React, { Component } from 'react'
import {
  ScrollView,
  Modal,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard
} from 'react-native'
//Libraries
import { TextField } from 'react-native-material-textfield'
import { connect } from 'react-redux'
//Assets
import { VALER_PURPLE, VALER_BACKGROUND, VALER_TEXTFIELD, VALER_TEXT } from '../../Assets/GlobalStyle'
import { ButtonCell } from '../Templates/ButtonCell'
import { Button } from '../Templates/Button'
import { changeEmail, changePassword, verifyPassword } from '../../Actions'
import { LoadWheel } from '../../Assets/LoadWheel'
import { Field, reduxForm, change, submit } from 'redux-form'
import { ReduxFormItemTextField } from '../../Assets/ReduxFormItemTextField.js'
let self
class LoginUserInfo extends Component {
  state = {
    email: '',
    password: '',
    currentEmail: '',
    newPassword: '',
    confirmPassword: '',
    newEmail: '',
    changePasswordmodal: false,
    newEmailModal: false,
    newPasswordModal: false,
    incorrectModal: false,
    loadingModal: false,
    loading: true,
    modalTitle: '',
    changeAction: '',
    emailAlert: false,
    passwordAlert: false,
    firstName: '',
    lastName: ''
  }

  static navigationOptions = {
    title: 'Login & User Details',
    headerStyle: { backgroundColor: VALER_PURPLE },
    headerTintColor: 'white',
    headerTitleStyle: { alignSelf: 'center' },
    headerRight: (<View></View>)
  };

  componentWillMount() {
    this.setState({
      userId: this.props.user && this.props.user.login_user ? this.props.user.login_user : '',
      currentEmail: this.props.user && this.props.user.email_id ? this.props.user.email_id : '',
      firstName: this.props.user && this.props.user.firstName ? this.props.user.firstName : '',
      lastName: this.props.user && this.props.user.lastName ? this.props.user.lastName : '',
    }, () => {
      // this.props.change('currentEmail',this.props.user.login_user);
      this.props.change();

    })

  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      email: '',
      password: '',
      newPassword: '',
      confirmPassword: '',
      newEmail: ''
    });
    this.setState({
      userId: nextProps.user && nextProps.user.login_user ? nextProps.user.login_user : '',
      currentEmail: nextProps.user && nextProps.user.email_id ? nextProps.user.email_id : '',
      firstName: nextProps.user && nextProps.user.firstName ? nextProps.user.firstName : '',
      lastName: nextProps.user && nextProps.user.lastName ? nextProps.user.lastName : '',
    }, () => {
      this.props.change('currentEmail', this.state.currentEmail);
    })
    this.setState({ loadingModal: false });

    if (nextProps.Settings.status == 'incorrect') {
      this.setState({ incorrectModal: true });
      this.props.dispatch({type:'REMOVE_SETTING_STATUS'})

    }
    else if (nextProps.Settings.status == 'correct') {
      if (this.state.changeAction == 'password_change')
        this.setState({ newPasswordModal: true })
          
      else
        this.setState({ newEmailModal: true });
        this.props.dispatch({type:'REMOVE_SETTING_STATUS'})

    }
    else if (nextProps.Settings.status == 'change_password_done') {
      this.setState({ passwordAlert: true })
      this.props.dispatch({type:'REMOVE_SETTING_STATUS'})

    }
    else if (nextProps.Settings.status == 'change_password_fail') {
      this.setState({ passwordAlert: true })
      this.props.dispatch({type:'REMOVE_SETTING_STATUS'})

    }
    else if (nextProps.Settings.status == 'change_email_done') {
      this.setState({ currentEmail: nextProps.user.email, emailAlert: true })
      this.props.dispatch({type:'REMOVE_SETTING_STATUS'})

      //this.setState({emailAlert:true})
    }
    self = this;

  }
  onSubmitButtonPress(name,values) {
    if(name == 'verify'){
      this.setState({ changePasswordmodal: false })
      this.props.verifyPassword({ password: values.currentPassword, userId: this.state.userId })
      this.setState({ loadingModal: true })
    }
    else if(name == 'confirm'){
      if(values.newPassword == values.confirmPassword){
              this.setState({ newPasswordModal: false })
              this.props.changePassword({ password: values.newPassword })
              this.setState({ loadingModal: true })
          } else {
           alert('Password is mismatch')
        }
    }
    // this.setState({ changePasswordmodal: false })
    // if (values.currentPassword) {
    //       console.log(values,"currentpassdd")
    //   this.props.verifyPassword({ password: values.password, userId: this.state.userId })
    //   this.setState({ loadingModal: true })
    // }
    // if(values.newPassword && (values.newPassword == values.confirmPassword)){
    //   this.setState({ newPasswordModal: false })
    //   this.props.changePassword({ password: values.newPassword })
    //   this.setState({ loadingModal: true })
    // } else {
    //   alert('Password is mismatch')
    // }
  }


  render() {
    let { handleSubmit } = this.props
    return (
      <ScrollView style={{ backgroundColor: VALER_BACKGROUND }}>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <KeyboardAvoidingView behavior='padding'>
            <Modal
              transparent={true}
              visible={this.state.changePasswordmodal}
              onRequestClose={() => { this.setState({ changePasswordmodal: false }) }}
            >
              <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>
                  <Text style={{ textAlign: 'center', margin: 10, fontSize: 18 }}> {this.state.modalTitle} </Text>
                  <Text style={{ textAlign: 'center', color: VALER_TEXTFIELD }}> Verify Your Current Password</Text>
                  <View style={{ borderColor: VALER_PURPLE, marginTop: 10, marginBottom: 10, borderRadius: 3, paddingLeft: 5, height: 44, justifyContent: 'center', borderWidth: 1 }}>
                    <Field
                      placeholder='Current Password'
                      secureTextEntry={true}
                      name="currentPassword"
                      value={this.state.password}
                      component={ReduxFormItemTextField}
                      autoCorrect={false}
                      underlineColorAndroid={'transparent'}
                      autoCapitalize={'none'}
                      returnKeyType={'done'}
                      textColor={VALER_TEXT}
                      onChangeText={password => this.setState({ password })}
                    />
                  </View>
                  <TouchableOpacity onPress={handleSubmit(this.onSubmitButtonPress.bind(this,'verify'))}>
                    <Button
                      Label={this.state.modalTitle}
                      BackgroundColor={{ backgroundColor: VALER_PURPLE }}
                      Color={{ color: 'white' }}
                    />
                  </TouchableOpacity>
                  <View style={{ margin: 5 }} />
                  <TouchableOpacity onPress={() => { this.setState({ changePasswordmodal: false }) }} >
                    <Button
                      Label='Cancel'
                      BackgroundColor={{ backgroundColor: 'grey' }}
                      Color={{ color: 'white' }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
            <LoadWheel
              visible={this.state.loadingModal}
              onRequestClose={() => this.setState({ loadingModal: false })}
              text='Verifying...'
            />
            <Modal
              transparent={true}
              visible={this.state.incorrectModal}
              onRequestClose={() => { this.setState({ incorrectModal: false }) }}>
              <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>
                  <Text style={{ textAlign: 'center', marginTop: 10, marginBottom: 20, fontSize: 18 }}> Incorrect Password </Text>
                  <TouchableOpacity onPress={() => { this.setState({ incorrectModal: false }) }} >
                    <Button
                      Label='OK'
                      BackgroundColor={{ backgroundColor: 'red' }}
                      Color={{ color: 'white' }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
            <Modal
              transparent={true}
              visible={this.state.newPasswordModal}
              onRequestClose={() => { this.setState({ newPasswordModal: false }) }}
            >
              <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>
                  <Text style={{ textAlign: 'center', margin: 10, fontSize: 18 }}> Change Password </Text>
                  <View style={{ borderColor: VALER_PURPLE, marginTop: 10, marginBottom: 10, borderRadius: 3, paddingLeft: 5, height: 44, justifyContent: 'center', borderWidth: 1 }}>
                    <Field
                      placeholder='New Password'
                      secureTextEntry={true}
                      name="newPassword"
                      value={this.state.newPassword}
                      autoCorrect={false}
                      component={ReduxFormItemTextField}
                      autoCapitalize={'none'}
                      underlineColorAndroid={'transparent'}
                      returnKeyType={'done'}
                      textColor={VALER_TEXT}
                      // onChangeText={newPassword => this.setState({ newPassword })}
                    />
                  </View>
                  <View style={{ borderColor: VALER_PURPLE, marginTop: 10, marginBottom: 10, borderRadius: 3, paddingLeft: 5, height: 44, justifyContent: 'center', borderWidth: 1 }}>
                    <Field
                      placeholder='Confirm Password'
                      secureTextEntry={true}
                      name="confirmPassword"
                      value={this.state.confirmPassword}
                      autoCorrect={false}
                      component={ReduxFormItemTextField}
                      returnKeyType={'done'}
                      underlineColorAndroid={'transparent'}
                      autoCapitalize={'none'}
                      textColor={VALER_TEXT}
                      // onChangeText={confirmPassword => this.setState({ confirmPassword })}
                    />
                  </View>
                  <TouchableOpacity onPress={handleSubmit(this.onSubmitButtonPress.bind(this,'confirm'))} >
                  <Button
                    Label='Change Password'
                    BackgroundColor={{ backgroundColor: VALER_PURPLE }}
                    Color={{ color: 'white' }}
                  />
                </TouchableOpacity>
                <View style={{ margin: 5 }} />
                <TouchableOpacity onPress={() => { this.setState({ newPasswordModal: false }) }} >
                  <Button
                    Label='Cancel'
                    BackgroundColor={{ backgroundColor: 'grey' }}
                    Color={{ color: 'white' }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Modal
            transparent={true}
            visible={this.state.newEmailModal}
            onRequestClose={() => { this.setState({ newEmailModal: false }) }}
          >
            <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>
                <Text style={{ textAlign: 'center', margin: 10, fontSize: 18 }}> Change Email </Text>
                <View style={{ borderColor: VALER_PURPLE, marginTop: 10, marginBottom: 10, borderRadius: 3, paddingLeft: 5, height: 44, justifyContent: 'center', borderWidth: 1 }}>
                  <Field
                    placeholder='New Email'
                    value={this.state.newEmail}
                    autoCorrect={false}
                    component={ReduxFormItemTextField}
                    autoCapitalize={'none'}
                    underlineColorAndroid={'transparent'}
                    textColor={VALER_TEXT}
                    onChangeText={newEmail => this.setState({ newEmail })}
                  />
                </View>
                <TouchableOpacity onPress={() => {
                  this.setState({ newEmailModal: false })
                  this.props.changeEmail({ email: this.state.newEmail })
                  this.setState({ loadingModal: true })
                }}>
                  <Button
                    Label='Change Email'
                    BackgroundColor={{ backgroundColor: VALER_PURPLE }}
                    Color={{ color: 'white' }}
                  />
                </TouchableOpacity>
                <View style={{ margin: 5 }} />
                <TouchableOpacity onPress={() => { this.setState({ newEmailModal: false }) }} >
                  <Button
                    Label='Cancel'
                    BackgroundColor={{ backgroundColor: 'grey' }}
                    Color={{ color: 'white' }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Modal
            transparent={true}
            visible={this.state.emailAlert}
            onRequestClose={() => { this.setState({ emailAlert: false }) }}
          >
            <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>

                <Text style={{ textAlign: 'center', margin: 10, fontSize: 18 }}> Email Successfully Changed... </Text>
                <TouchableOpacity onPress={() => {
                  this.setState({ emailAlert: false })
                }} >
                  <Button
                    Label='OK'
                    BackgroundColor={{ backgroundColor: VALER_PURPLE }}
                    Color={{ color: 'white' }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Modal
            transparent={true}
            visible={this.state.passwordAlert}
            onRequestClose={() => { this.setState({ passwordAlert: false }) }}
          >
            <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ backgroundColor: 'white', width: 300, padding: 20, borderRadius: 5 }}>

                <Text style={{ textAlign: 'center', margin: 10, fontSize: 18 }}> Password Successfully Changed... </Text>
                <TouchableOpacity onPress={() => {
                  this.setState({ passwordAlert: false })
                }} >
                  <Button
                    Label='OK'
                    BackgroundColor={{ backgroundColor: VALER_PURPLE }}
                    Color={{ color: 'white' }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Text style={{ color: VALER_TEXTFIELD, marginLeft: 10, marginBottom: 3, paddingTop: 10, fontSize: 13 }}>Login Info</Text>
          <View style={{ paddingLeft: 10, backgroundColor: 'white' }}>
            <Field
              label='Email'
              name='currentEmail'
              value={this.state.currentEmail}
              disabled={true}
              baseColor={VALER_TEXTFIELD}
              tintColor={VALER_PURPLE}
              textColor={VALER_TEXT}
              labelHeight={16}
              component={ReduxFormItemTextField}
              labelPadding={0}
              underlineColorAndroid={'transparent'}
              inputContainerPadding={4}
              autoCorrect={false}
              autoCapitalize={'none'}
              editable={false}
              onSubmitEditing={((event) => { this.refs.password.focus() })}
              returnKeyType={'next'}
            />
          </View>
          <View style={{}} />
          <TouchableOpacity onPress={() => this.setState({ changePasswordmodal: true, modalTitle: 'Verify Password', changeAction: 'email_change' })}>
            <ButtonCell
              Label='Change Email'
              Style={{ color: 'blue' }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({ changePasswordmodal: true, modalTitle: 'Change Password', changeAction: 'password_change' })}>
            <ButtonCell
              Label='Change Password'
              Style={{ color: 'blue' }}
            />
          </TouchableOpacity>
          <Text style={{ color: VALER_TEXTFIELD, marginLeft: 10, marginBottom: 3, paddingTop: 10, fontSize: 13 }}>User Info</Text>
          <View style={{ flexDirection: 'row', paddingLeft: 10, backgroundColor: 'white' }}>
            <Field
              label='First Name'
              name='firstName'
              value={this.state.firstName}
              disabled={true}
              baseColor={VALER_TEXTFIELD}
              tintColor={VALER_PURPLE}
              textColor={VALER_TEXT}
              component={ReduxFormItemTextField}
              labelHeight={16}
              underlineColorAndroid={'transparent'}
              labelPadding={0}
              inputContainerPadding={4}
              autoCorrect={false}
              containerStyle={{ flex: 1 }}
              ref='firstname'
              editable={false}
              onSubmitEditing={((event) => { this.refs.businessusername.focus() })}
              returnKeyType={'next'}
            />
            <View style={{ marginLeft: 10, flex: 1 }}>
              <Field
                label='Last Name'
                name='lastName'
                value={this.state.lastName}
                disabled={true}
                baseColor={VALER_TEXTFIELD}
                tintColor={VALER_PURPLE}
                textColor={VALER_TEXT}
                labelHeight={16}
                underlineColorAndroid={'transparent'}
                labelPadding={0}
                inputContainerPadding={4}
                component={ReduxFormItemTextField}
                autoCorrect={false}
                containerStyle={{ flex: 1 }}
                ref='lastname'
                editable={false}
                onSubmitEditing={((event) => { this.refs.businessusername.focus() })}
                returnKeyType={'next'}
              />
            </View>
          </View>
          <View style={{ marginBottom: 30 }} />
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </ScrollView >
  )
  }
}
const mapStateToProps = ({ Settings, auth }) => {
  return {
    Settings: Settings,
    user: auth.user,
    initialValues: {
      currentEmail: auth.user.email_id,
      firstName: auth.user.firstName,
      lastName: auth.user.lastName
    }
  };
}
LoginUserInfo = reduxForm({
  form: 'loginUserInfo',
  fields: ['currentEmail', 'firstName', 'lastName'],
  enableReinitialize: true,
  destroyOnUnmount: false,
  // initialValues: {currentEmail: "abc", firstName: "b", testRadio:"y", testIn2: "def", testSelect2: "d", testRadio2:"j"},
  onSubmit: function (values, dispatch, props) {
    self.onSubmitButtonPress(values);
  }
})(LoginUserInfo);


export default connect(mapStateToProps, { changeEmail, changePassword, verifyPassword, change })(LoginUserInfo);
