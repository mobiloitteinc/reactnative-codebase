'use strict'
//React
import React from 'react'
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  SectionList,
  FlatList,
  AsyncStorage
} from 'react-native'
//Libraries
import { connect } from 'react-redux'
import firebase from 'react-native-firebase'
import { NavigationActions } from 'react-navigation'
//Assets
import { logOutRequest,removePersistedData } from '../../Actions';
import { LoadWheel } from '../../Assets/LoadWheel'

import { VALER_PURPLE, VALER_TABBAR, VALER_BACKGROUND, VALER_CELLBORDER } from '../../Assets/GlobalStyle'
import { PayoutCell } from '../Templates/PayoutCell'
import { SettingsHeader } from '../Templates/SettingsHeader'
import { SettingsCell } from '../Templates/SettingsCell'
import { ButtonCell } from '../Templates/ButtonCell'
import { BusinessCell } from '../Templates/BusinessCell'

const touch = require('../../Assets/Settings/fingerprint.png')
const personal = require('../../Assets/Settings/personal-details.png')
const bank = require('../../Assets/Settings/bank-settings.png')
const discounts = require('../../Assets/Settings/discounts.png')
const online = require('../../Assets/Settings/online-defaults.png')
const businessicon = require('../../Assets/Settings/business.png')
const tax = require('../../Assets/Settings/tax.png')

let account = [
  { Name: "Login & User Details", key: 0, source: personal },
  { Name: "Touch ID & PIN", key: 1, source: touch },
]
let business = [
  { Name: "Business Details", key: 0, source: businessicon },
  { Name: "Bank Accounts", key: 1, source: bank },
  { Name: "Online Defaults", key: 2, source: online },
]
let inventory = [
  { Name: "Discounts", key: 0, source: discounts },
  { Name: "Sales Tax", key: 1, source: tax },
]


class Settings extends React.Component {

  static navigationOptions = {
    title: 'Settings',
    headerStyle: { backgroundColor: VALER_PURPLE },
    headerTintColor: 'white',
    headerBackTitle: null,
    headerTitleStyle: { alignSelf: 'center' },
  };

  state = {
    businessName: '',
    businessUsername: '',
    verifyPin: false,
    userId: '',
    enabledPin: false,
    loading: false
  }

  componentWillMount() {
    this.setState({
      userId: this.props.user && this.props.user.login_user ? this.props.user.login_user : '',
      enabledPin: this.props.user && this.props.user.enabledPin ? this.props.user.enabledPin : '',
      verifyPin: this.props.navigation.state.params && this.props.navigation.state.params.verifyPinScreen ? this.props.navigation.state.params.verifyPinScreen : false,
      businessName: this.props.user && this.props.user.businessName ? this.props.user.businessName : '',
      businessUsername: this.props.user && this.props.user.businessUsername ? this.props.user.businessUsername : '',
    }, () => {
      if (this.state.enabledPin) {
        //this.props.navigation.navigate('ConfirmPin', { passcode: this.state.pin, userId: this.state.userId });
      }
    })

  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      businessName: nextProps.user && nextProps.user.businessName ? nextProps.user.businessName : '',
      businessUsername: nextProps.user && nextProps.user.businessUsername ? nextProps.user.businessUsername : '',
    })
    if(nextProps.logoutsuccess == true){
      // this.props.navigation.goBack(null);
      // this.setState({loading : false})
      // this.props.navigation.navigate('Auth');
      //  setTimeout(() => {
      //    this.setState({loading : false})
      //    this.props.navigation.navigate('Auth');
      //  },200)
    }
  }
  renderHeader = ({ section }) => {
    return (
      <SettingsHeader
        Label={section.title}
      />
    )
  }

  renderSettingItem = ({ section, item }) => {
    return (
      <TouchableOpacity onPress={() => this.handleSetting(section, item)}>
        <SettingsCell
          Label={item.Name}
          source={item.source}
        />
      </TouchableOpacity>
    )
  }
  async logOutUser() {
    await  AsyncStorage.removeItem('reduxPersist:auth');
    await firebase.auth().signOut();
    await this.props.logOutRequest()
    // this.props.navigation.goBack(null);
    this.props.navigation.goBack(null);
    //this.props.navigation.navigate('Auth');
    this.props.navigation.navigate('Main');
    AsyncStorage.removeItem("LoginStatus");
    //  this.setState({loading:true},async () => {
    //  await this.props.logOutRequest()
    //   await firebase.auth().signOut();
    //   // this.props.removePersistedData();
     
    //  })
  }
  renderBusiness = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => console.log('hey there')}>
        <BusinessCell
          Label={item.Name}
        />
      </TouchableOpacity>
    )
  }

  handleSetting(section, item) {
    // alert((item.key))
    if (section.key === 0) {
      if (item.key === 0) {
        this.props.navigation.navigate('LoginUserInfo')
        this.props.navigation.setParams({ visible: false })
      } else if (item.key === 1) {
        this.props.navigation.navigate('TouchIDPIN')
      }
    } else if (section.key === 1) {
      if (item.key === 0) {
        this.props.navigation.navigate('BusinessOwnerInfo')
      } else if (item.key === 1) {
        this.props.navigation.navigate('BankAccounts')
      } else if (item.key === 2) {
        this.props.navigation.navigate('OnlineDefaults')
      }
    } else if (section.key === 2) {
      if (item.key === 0) {
        this.props.navigation.navigate('Discounts')
      } else if (item.key === 1) {
        this.props.navigation.navigate('SalesTax')
      }
    }
  }

  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: VALER_BACKGROUND }}>
        <View style={{ backgroundColor: VALER_TABBAR }}>
          <View style={{ flexDirection: 'row' }}>
            <Image style={{ height: 90, width: 90, backgroundColor: 'red', margin: 5, borderRadius: 45 }} />
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={{ fontSize: 20 }} numberOfLines={1} ellipsizeMode='tail'>{this.state.businessName}</Text>
              <Text style={{ fontSize: 17, color: VALER_PURPLE }} numberOfLines={1} ellipsizeMode='tail'>@{this.state.businessUsername}</Text>
              <Text style={{ fontSize: 17, color: '#8E8E93' }} numberOfLines={1} ellipsizeMode='tail'>Retail Store</Text>
            </View>
          </View>
          <View style={{ flex: 1, marginLeft: 5, flexDirection: 'row' }}>
            <Text style={{ color: VALER_PURPLE, fontSize: 17 }} numberOfLines={1} ellipsizeMode='tail'>Available Balance: </Text>
            <View style={{ flex: 1 }}>
              <Text style={{ color: VALER_PURPLE, fontSize: 17 }} numberOfLines={1} ellipsizeMode='tail'>$2,234.32</Text>
              <Text style={{ color: '#8E8E93', fontSize: 13 }} numberOfLines={1} ellipsizeMode='tail'>$120.10 pending</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('BankTransfers')}>
          <PayoutCell />
        </TouchableOpacity>
        <View>
          <SectionList
            renderSectionHeader={this.renderHeader}
            ItemSeparatorComponent={() => <View style={{ flex: 1, height: 1, backgroundColor: VALER_CELLBORDER, marginLeft: 40 }} />}
            scrollEnabled={false}
            style={{ backgroundColor: 'white' }}
            sections={[
              {
                title: "Account & Security",
                key: 0,
                data: account,
                renderItem: this.renderSettingItem,
              },
              {
                title: "Business & Bank",
                key: 1,
                data: business,
                renderItem: this.renderSettingItem
              },
              {
                title: "Inventory",
                key: 2,
                data: inventory,
                renderItem: this.renderSettingItem,
              },
            ]}
          />
        </View>
        <SettingsHeader
          Label="Switch Business"
        />
        <FlatList
          data={[{ Name: 'Shop Something else' }, { Name: 'Shop Something else' }]}
          renderItem={this.renderBusiness}
          style={{ backgroundColor: 'white' }}
          keyExtractor={(item, index) =>`${index}shop1`}

          ItemSeparatorComponent={() => <View style={{ flex: 1, height: 1, backgroundColor: VALER_CELLBORDER, marginLeft: 40 }} />}
        />
        <TouchableOpacity>
          <ButtonCell
            section='2'
            key='0'
            Label="Add Business"
            Style={{ color: '#007AFF' }}
          />
        </TouchableOpacity>
        <View style={{ marginBottom: 20 }} />
        <TouchableOpacity>
          <ButtonCell
            section='2'
            key='1'
            Label="Support & Legal"
            Style={{ color: '#007AFF' }}
          />
        </TouchableOpacity>
        <View style={{ marginBottom: 20 }} />
        <TouchableOpacity onPress={this.logOutUser.bind(this)}
        >
          <ButtonCell
            section='2'
            key='2'
            Label="Log Out"
            Style={{ color: '#FF3B30' }}
          />
        </TouchableOpacity>
       <LoadWheel visible={this.state.loading} 
         onRequestClose={() => this.setState({ loading:false })} text={'Logout...'}/>
      
        <View style={{ marginBottom: 30 }} />
      </ScrollView>
    )
  }
}
const mapStateToProps = ({ state, action, auth }) => {
  return {
    state: state,
    action: action,
    user: auth.user,
    logoutsuccess:auth.logoutsuccess
  }
}
export default connect(mapStateToProps, { logOutRequest,removePersistedData })(Settings);
